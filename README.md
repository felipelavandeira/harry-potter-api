# Aplicação CRUD para personagens de Harry Potter

Aplicação desenvolvida para desafio técnico

## Pré Requisitos
O projeto foi desenvolvido com as tecnologias:

- JDK 11;
- Maven 3.6.3 (ou Maven Wrapper do Spring Boot)
- Banco de dados PostgreSQL 13.4

## Configuração
É necessário configurar as propriedades abaixo em um arquivo application.properties:

```
spring.jpa.database=POSTGRESQL
spring.sql.init.platform=postgres
spring.jpa.show-sql=true
spring.jpa.hibernate.ddl-auto=none
spring.datasource.url=jdbc:postgresql://localhost:5432/NOME_DO_BANCO
spring.datasource.username=USUÁRIO_BANCO
spring.datasource.password=SENHA_USUÁRIO_BANCO
server.port=8080
```

> As tabelas serão criadas automaticamente no primeiro start da aplicação

## Execução

para criação de uma imagem executável do projeto, basta executar o comando Maven:

`mvn clean package install`

OU:

`./mvnw clean package install` (Caso esteja utilizando o maven wrapper)

### Criação dos objetos de CASA

A aplicação, quando executada pela primeira vez, segue os passos de:

1) Busca todos os itens na API: [http://us-central1-rh-challenges.cloudfunctions.net/potterApi/houses](http://us-central1-rh-challenges.cloudfunctions.net/potterApi/houses)
2) Converte os itens encontrados na [API](http://us-central1-rh-challenges.cloudfunctions.net/potterApi/houses) em itens na tabela `houses` na base da aplicação

## Endpoints

O endpoint disponível na aplicação é:

### _/characters_

#### Listagem
O endpoint de listagem dos personagens é `http://localhost:8080/characters/`

A listagem pode ser ordenada, filtrada e paginada com os atributos:

| Atributo | Tipo | Descrição | Exemplo |    
|--|--|--|--|    
| page | int | Define qual é a página atual da lista de personagens. A primeira página é definida pelo número 0. | 0    
| size | int | Define quantos elementos serão listados na página atual | 5    
| sort | string | Define qual será o atributo base para a ordenação, seguido do tipo da ordenação, podendo ser ASC (crescente) ou DESC (decrescente) | name,DESC
| house | string (UUID) | Faz um refinamento na busca por personagens pertencentes à uma determinada casa | house=1760529f-6d51-4cb1-bcb1-25087fce5bde

#### GET
O endpoint para a consulta de um personagem é o `http://localhost:8080/characters/{id}`

> Onde *{id}* deve conter o ID de um personagem válido.

#### POST
O endpoint de criação de um personagem é o `http://localhost:8080/characters/`

A requisição **POST** deve enviar um _body_ como o abaixo:

```
{
    "name": "Teste",
    "role": "STUDENT",
    "school": "Hogwarts",
    "house": {
        "id": "56cabe3a-9bce-4b83-ba63-dcd156e9be46"
    },
    "patronus": "expecto"
}
```

> O atributo `house` deve conter um id de uma CASA válida
> 
> Os ID's podem ser encontrados na [API](http://us-central1-rh-challenges.cloudfunctions.net/potterApi/houses). (Os objetos persistidos no banco de dados local serão idênticos aos encontrados na API, incluindo o ID)

#### PUT
O endpoint de edição de um personagem é o `http://localhost:8080/characters/{id}`

> Onde *{id}* deve conter o ID de um personagem válido.

A requisição **PUT** deve enviar um _body_ como o abaixo:

```
{
    "name": "Teste",
    "role": "STUDENT",
    "school": "Hogwarts",
    "house": {
        "id": "56cabe3a-9bce-4b83-ba63-dcd156e9be46"
    },
    "patronus": "expecto"
}
```

> O atributo `house` deve conter um id de uma CASA válida
>
> Os ID's podem ser encontrados na [API](http://us-central1-rh-challenges.cloudfunctions.net/potterApi/houses). (Os objetos persistidos no banco de dados local serão idênticos aos encontrados na API, incluindo o ID)

#### DELETE
O endpoint de exclusão de um personagem é o  `http://localhost:8080/characters/{id}`

> Onde *{id}* deve conter o ID de um produto válido.