package br.com.felipelavandeira.harrypotterapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesteHarryPotterApplication {

    public static void main(String[] args) {
        SpringApplication.run(TesteHarryPotterApplication.class, args);
    }

}
