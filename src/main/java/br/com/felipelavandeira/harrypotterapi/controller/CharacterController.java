package br.com.felipelavandeira.harrypotterapi.controller;

import br.com.felipelavandeira.harrypotterapi.model.dto.CharacterDTO;
import br.com.felipelavandeira.harrypotterapi.model.entity.Character;
import br.com.felipelavandeira.harrypotterapi.service.AlterCharacterService;
import br.com.felipelavandeira.harrypotterapi.service.RetrieveCharacterService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.UUID;

@RestController
@RequestMapping("/characters")
public class CharacterController {

    @Inject
    private RetrieveCharacterService retrieveService;

    @Inject
    private AlterCharacterService alterCharacterService;

    @GetMapping
    public ResponseEntity<Page<CharacterDTO>> index(@Param("house") String houseId, Pageable pageable){
        Page<CharacterDTO> characters = retrieveService.retrieveList(pageable, houseId);
        return !characters.isEmpty() ? ResponseEntity.ok(characters) : ResponseEntity.noContent().build();
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<CharacterDTO> get(@PathVariable("id") UUID id){
        return ResponseEntity.ok(retrieveService.retrieveOne(id));
    }

    @PostMapping
    public ResponseEntity<CharacterDTO> create(@RequestBody Character character){
        CharacterDTO dto = alterCharacterService.createCharacter(character);
        return ResponseEntity.ok(dto);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<CharacterDTO> update(@PathVariable("id") UUID id, @RequestBody Character character){
        CharacterDTO dto = alterCharacterService.updateCharacter(id, character);
        return ResponseEntity.ok(dto);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") UUID id){
        alterCharacterService.deleteCharacter(id);
        return ResponseEntity.ok().build();
    }

}
