package br.com.felipelavandeira.harrypotterapi.exceptionhandler.handler;

import br.com.felipelavandeira.harrypotterapi.exceptionhandler.dto.ErrorDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.inject.Inject;
import java.util.Locale;

@ControllerAdvice
@Slf4j
public abstract class DefaultExceptionHandler extends ResponseEntityExceptionHandler {

    @Inject
    protected MessageSource messageSource;

    @ExceptionHandler(value = {RuntimeException.class})
    public ResponseEntity<Object> handleDefaultException(RuntimeException exception, WebRequest request){
        log.error(getUserMessage(), exception);
        return handleExceptionInternal(
                exception,
                new ErrorDTO(getUserMessage().concat(getComplementaryMessage()), exception.getMessage()),
                new HttpHeaders(),
                getStatus(),
                request
        );
    }

    protected Locale getLocale(){
        return LocaleContextHolder.getLocale();
    }

    protected String getComplementaryMessage(){
        return messageSource.getMessage("harry_potter_api.try_again", null, getLocale());
    }

    protected abstract String getUserMessage();
    protected abstract HttpStatus getStatus();
}
