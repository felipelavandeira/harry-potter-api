package br.com.felipelavandeira.harrypotterapi.exceptionhandler.handler;

import br.com.felipelavandeira.harrypotterapi.exceptionhandler.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class NotFoundExceptionHandler extends DefaultExceptionHandler{

    @Override
    @ExceptionHandler(value = {NotFoundException.class})
    public ResponseEntity<Object> handleDefaultException(RuntimeException exception, WebRequest request) {
        return super.handleDefaultException(exception, request);
    }

    @Override
    protected String getUserMessage() {
        return messageSource.getMessage("harry_potter_api.not_found_error", null, getLocale());
    }

    @Override
    protected HttpStatus getStatus() {
        return HttpStatus.NOT_FOUND;
    }
}
