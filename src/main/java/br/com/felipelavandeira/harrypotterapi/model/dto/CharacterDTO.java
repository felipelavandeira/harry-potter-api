package br.com.felipelavandeira.harrypotterapi.model.dto;

import br.com.felipelavandeira.harrypotterapi.model.entity.House;
import br.com.felipelavandeira.harrypotterapi.model.enumerated.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class CharacterDTO {

    private UUID id;
    private String name;
    private String role;
    private String school;
    private String house;
    private String patronus;

}
