package br.com.felipelavandeira.harrypotterapi.model.enumerated;

public enum Role {

    STUDENT("Student"),
    TEACHER("Teacher");

    private final String description;

    Role(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
