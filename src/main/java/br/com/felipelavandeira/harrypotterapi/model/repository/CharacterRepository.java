package br.com.felipelavandeira.harrypotterapi.model.repository;

import br.com.felipelavandeira.harrypotterapi.model.entity.Character;
import br.com.felipelavandeira.harrypotterapi.model.entity.House;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CharacterRepository extends JpaRepository<Character, UUID> {

    Page<Character> findByHouse(Pageable pageable, House house);
}
