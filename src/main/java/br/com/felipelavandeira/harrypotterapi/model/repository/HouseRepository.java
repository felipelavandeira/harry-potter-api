package br.com.felipelavandeira.harrypotterapi.model.repository;

import br.com.felipelavandeira.harrypotterapi.model.entity.House;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface HouseRepository extends JpaRepository<House, UUID> {
}
