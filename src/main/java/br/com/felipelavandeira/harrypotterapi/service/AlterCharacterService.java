package br.com.felipelavandeira.harrypotterapi.service;

import br.com.felipelavandeira.harrypotterapi.model.dto.CharacterDTO;
import br.com.felipelavandeira.harrypotterapi.model.entity.Character;

import java.util.UUID;

public interface AlterCharacterService {

    CharacterDTO createCharacter(Character character);

    CharacterDTO updateCharacter(UUID id, Character character);

    void deleteCharacter(UUID id);
}
