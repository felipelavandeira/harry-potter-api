package br.com.felipelavandeira.harrypotterapi.service;

import br.com.felipelavandeira.harrypotterapi.model.entity.Character;

public interface PrepareCharacterService {

    Character prepareForUpdate(Character character, Character persisted);

    Character prepareForCreate(Character character);

}
