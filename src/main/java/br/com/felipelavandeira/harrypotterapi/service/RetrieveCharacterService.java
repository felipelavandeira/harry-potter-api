package br.com.felipelavandeira.harrypotterapi.service;

import br.com.felipelavandeira.harrypotterapi.model.dto.CharacterDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface RetrieveCharacterService {

    Page<CharacterDTO> retrieveList(Pageable pageable, String houseId);

    CharacterDTO retrieveOne(UUID id);

}
