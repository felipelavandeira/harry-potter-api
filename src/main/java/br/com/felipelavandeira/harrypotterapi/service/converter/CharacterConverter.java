package br.com.felipelavandeira.harrypotterapi.service.converter;

import br.com.felipelavandeira.harrypotterapi.model.dto.CharacterDTO;
import br.com.felipelavandeira.harrypotterapi.model.entity.Character;

public class CharacterConverter {

    private CharacterConverter() {
    }

    public static CharacterDTO convertToDTO(Character character){
        CharacterDTO dto = new CharacterDTO();
        dto.setId(character.getId());
        dto.setName(character.getName());
        dto.setRole(character.getRole().getDescription());
        dto.setSchool(character.getSchool());
        dto.setHouse(character.getHouse().getName());
        dto.setPatronus(character.getPatronus());
        return dto;
    }

}
