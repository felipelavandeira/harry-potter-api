package br.com.felipelavandeira.harrypotterapi.service.impl;

import br.com.felipelavandeira.harrypotterapi.exceptionhandler.exception.BadRequestException;
import br.com.felipelavandeira.harrypotterapi.exceptionhandler.exception.NotFoundException;
import br.com.felipelavandeira.harrypotterapi.model.dto.CharacterDTO;
import br.com.felipelavandeira.harrypotterapi.model.entity.Character;
import br.com.felipelavandeira.harrypotterapi.model.repository.CharacterRepository;
import br.com.felipelavandeira.harrypotterapi.service.AlterCharacterService;
import br.com.felipelavandeira.harrypotterapi.service.PrepareCharacterService;
import br.com.felipelavandeira.harrypotterapi.service.converter.CharacterConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.UUID;

@Service
@Slf4j
public class AlterCharacterServiceImpl implements AlterCharacterService {

    @Inject
    private CharacterRepository repository;

    @Inject
    private PrepareCharacterService prepareService;

    @Override
    @Transactional
    public CharacterDTO createCharacter(Character requestCharacter) {
        requestCharacter = prepareService.prepareForCreate(requestCharacter);
        try {
            requestCharacter = repository.save(requestCharacter);
            return CharacterConverter.convertToDTO(requestCharacter);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new BadRequestException(e.getMessage());
        }
    }

    @Override
    @Transactional
    public CharacterDTO updateCharacter(UUID id, Character character) {
        Character persisted = getCharacter(id);
        persisted = prepareService.prepareForUpdate(character, persisted);
        return CharacterConverter.convertToDTO(repository.save(persisted));
    }

    @Override
    @Transactional
    public void deleteCharacter(UUID id) {
        Character character = getCharacter(id);
        try {
            repository.delete(character);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new BadRequestException(e.getMessage());
        }
    }

    private Character getCharacter(UUID id) {
        return repository.findById(id).orElseThrow(NotFoundException::new);
    }
}
