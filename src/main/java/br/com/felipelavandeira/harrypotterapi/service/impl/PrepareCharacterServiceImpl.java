package br.com.felipelavandeira.harrypotterapi.service.impl;

import br.com.felipelavandeira.harrypotterapi.exceptionhandler.exception.NotFoundHouseException;
import br.com.felipelavandeira.harrypotterapi.model.entity.Character;
import br.com.felipelavandeira.harrypotterapi.model.entity.House;
import br.com.felipelavandeira.harrypotterapi.model.enumerated.Role;
import br.com.felipelavandeira.harrypotterapi.model.repository.HouseRepository;
import br.com.felipelavandeira.harrypotterapi.service.PrepareCharacterService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Objects;

@Service
public class PrepareCharacterServiceImpl implements PrepareCharacterService {

    @Inject
    private HouseRepository repository;

    @Override
    public Character prepareForUpdate(Character character, Character persisted) {
        updateCharacterAttributes(character, persisted);
        return persisted;
    }

    @Override
    public Character prepareForCreate(Character character) {
        character.setHouse(getHouse(character));
        return character;
    }

    private void updateCharacterAttributes(Character character, Character persisted) {
        House house = getHouse(character);
        if (Objects.nonNull(house)){
            persisted.setHouse(house);
        }

        String name = character.getName();
        if (Objects.nonNull(name)){
            persisted.setName(name);
        }

        String patronus = character.getPatronus();
        if (Objects.nonNull(patronus)){
            persisted.setPatronus(patronus);
        }

        Role role = character.getRole();
        if (Objects.nonNull(role)){
            persisted.setRole(role);
        }

        String school = character.getSchool();
        if (Objects.nonNull(school)) {
            persisted.setSchool(school);
        }
    }

    private House getHouse(Character character) {
        House house = character.getHouse();
        if (Objects.isNull(house)){
            return null;
        }
        house = repository.findById(house.getId()).orElseThrow(NotFoundHouseException::new);
        return house;
    }
}
