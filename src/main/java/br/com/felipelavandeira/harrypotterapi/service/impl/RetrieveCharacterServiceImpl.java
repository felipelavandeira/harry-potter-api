package br.com.felipelavandeira.harrypotterapi.service.impl;

import br.com.felipelavandeira.harrypotterapi.exceptionhandler.exception.NotFoundException;
import br.com.felipelavandeira.harrypotterapi.model.dto.CharacterDTO;
import br.com.felipelavandeira.harrypotterapi.model.entity.Character;
import br.com.felipelavandeira.harrypotterapi.model.entity.House;
import br.com.felipelavandeira.harrypotterapi.model.repository.CharacterRepository;
import br.com.felipelavandeira.harrypotterapi.service.RetrieveCharacterService;
import br.com.felipelavandeira.harrypotterapi.service.converter.CharacterConverter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.Objects;
import java.util.UUID;

@Service
public class RetrieveCharacterServiceImpl implements RetrieveCharacterService {

    @Inject
    private CharacterRepository repository;

    @Override
    @Transactional(readOnly = true)
    public Page<CharacterDTO> retrieveList(Pageable pageable, String houseId) {
        if (Objects.isNull(houseId)){
            return repository.findAll(pageable).map(CharacterConverter::convertToDTO);
        }
        House house = new House();
        house.setId(UUID.fromString(houseId));
        return repository.findByHouse(pageable, house).map(CharacterConverter::convertToDTO);
    }

    @Override
    @Transactional(readOnly = true)
    public CharacterDTO retrieveOne(UUID id) {
        Character character = repository.findById(id).orElseThrow(NotFoundException::new);
        return CharacterConverter.convertToDTO(character);
    }

}
