package db.migration;

import db.migration.dto.HouseDTO;
import db.migration.dto.HousesResultDTO;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.springframework.http.HttpRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;

public class V2_0_0__populate_houses_with_api extends BaseJavaMigration {

    @Override
    public void migrate(Context context) throws Exception {
        HousesResultDTO apiResults = getApiResults();
        Statement statement = context.getConnection().createStatement();
        apiResults.getHouses().forEach(item ->{
            try {
                statement.execute(buildSQL(item));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private String buildSQL(HouseDTO item) {
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO houses (id, house_name, school, house_ghost, mascot, head_of_house, founder, colors, house_values) ");
        query.append("VALUES (");
        query.append("'").append(item.getId()).append("', ");
        query.append("'").append(item.getName()).append("', ");
        query.append("'").append(item.getSchool()).append("', ");
        query.append("'").append(item.getHouseGhost()).append("', ");
        query.append("'").append(item.getMascot()).append("', ");
        query.append("'").append(item.getHeadOfHouse()).append("', ");
        query.append("'").append(item.getFounder()).append("', ");
        query.append("'").append(String.join(", ", item.getColors())).append("', ");
        query.append("'").append(String.join(", ", item.getValues())).append("'");
        query.append(");");
        return query.toString();
    }

    private HousesResultDTO getApiResults() {
        String url = "http://us-central1-rh-challenges.cloudfunctions.net/potterApi/houses/";
        RestTemplate restTemplate = new RestTemplate();
        buildRequestHeaders(restTemplate);
        ResponseEntity<HousesResultDTO> response = restTemplate.getForEntity(url, HousesResultDTO.class);
        return response.getBody();
    }

    private void buildRequestHeaders(RestTemplate restTemplate) {
        restTemplate.getInterceptors().add(new ClientHttpRequestInterceptor() {
            @Override
            public ClientHttpResponse intercept(HttpRequest request, byte[] bytes, ClientHttpRequestExecution execution) throws IOException {
                String apiKey = "ddd0ee8e-69ce-4471-8b32-3356b666e48f";
                request.getHeaders().add("apikey", apiKey);
                return execution.execute(request, bytes);
            }
        });
    }


}
