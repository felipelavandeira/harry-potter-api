package db.migration.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class HouseDTO {

    private String id;
    private String houseGhost;
    private String mascot;
    private String founder;
    private List<String> values;
    private List<String> colors;
    private String headOfHouse;
    private String name;
    private String school;

}
