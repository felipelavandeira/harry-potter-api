-- Create character table
CREATE TABLE characters
(
    id       UUID
        CONSTRAINT character_pk
            PRIMARY KEY,
    name     VARCHAR(255),
    role     VARCHAR(10)  NOT NULL,
    school   VARCHAR(255) NOT NULL,
    house    UUID         NOT NULL,
    patronus VARCHAR(50)
);