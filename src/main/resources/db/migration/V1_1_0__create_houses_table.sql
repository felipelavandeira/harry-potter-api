CREATE TABLE houses
(
    id            UUID    NOT NULL
        CONSTRAINT houses_pk
            PRIMARY KEY,
    house_name    VARCHAR NOT NULL,
    school        VARCHAR NOT NULL,
    house_ghost   VARCHAR,
    mascot        VARCHAR,
    head_of_house VARCHAR,
    founder       VARCHAR,
    colors        VARCHAR,
    house_values        VARCHAR
);

