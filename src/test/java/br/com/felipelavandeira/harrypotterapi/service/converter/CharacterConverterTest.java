package br.com.felipelavandeira.harrypotterapi.service.converter;

import br.com.felipelavandeira.harrypotterapi.model.dto.CharacterDTO;
import br.com.felipelavandeira.harrypotterapi.model.entity.Character;
import br.com.felipelavandeira.harrypotterapi.model.entity.House;
import br.com.felipelavandeira.harrypotterapi.model.enumerated.Role;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CharacterConverterTest {

    private Character character;
    private UUID id;

    @BeforeEach
    void setUp() {
        id = UUID.randomUUID();
        House house = new House();
        house.setName("Gryffindor");

        character = new Character();
        character.setId(id);
        character.setHouse(house);
        character.setSchool("Hogwarts");
        character.setRole(Role.STUDENT);
        character.setPatronus("expectro");
        character.setName("Harry Potter");
    }

    @Test
    void testConvertToDTO() {
        CharacterDTO dto = assertDoesNotThrow(() -> CharacterConverter.convertToDTO(character));
        assertEquals(id, dto.getId());
        assertEquals(character.getHouse().getName(), dto.getHouse());
        assertEquals(character.getSchool(), dto.getSchool());
        assertEquals(character.getRole().getDescription(), dto.getRole());
        assertEquals(character.getPatronus(), dto.getPatronus());
        assertEquals(character.getName(), dto.getName());
    }
}