package br.com.felipelavandeira.harrypotterapi.service.impl;

import br.com.felipelavandeira.harrypotterapi.exceptionhandler.exception.BadRequestException;
import br.com.felipelavandeira.harrypotterapi.exceptionhandler.exception.NotFoundException;
import br.com.felipelavandeira.harrypotterapi.model.entity.Character;
import br.com.felipelavandeira.harrypotterapi.model.enumerated.Role;
import br.com.felipelavandeira.harrypotterapi.model.repository.CharacterRepository;
import br.com.felipelavandeira.harrypotterapi.service.PrepareCharacterService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static utils.CharacterComparator.compareCharacterResults;
import static utils.CharacterCreator.createHarryPotter;

@ExtendWith(MockitoExtension.class)
class AlterCharacterServiceImplTest {

    @InjectMocks
    private AlterCharacterServiceImpl alterCharacterService;

    @Mock
    private CharacterRepository repository;

    @Mock
    private PrepareCharacterService prepareService;

    @Captor
    private ArgumentCaptor<Character> characterCaptor;

    @Test
    void testCreateCharacter() {
        Character character = createHarryPotter();
        when(prepareService.prepareForCreate(any(Character.class))).thenReturn(character);
        when(repository.save(any())).thenReturn(character);
        alterCharacterService.createCharacter(character);
        verify(repository).save(characterCaptor.capture());
        Character value = characterCaptor.getValue();
        compareCharacterResults(character, value);
    }

    @Test
    void testCreateCharacterWithException() {
        Character character = createHarryPotter();
        when(prepareService.prepareForCreate(any(Character.class))).thenReturn(character);
        when(repository.save(any())).thenThrow(new RuntimeException());
        assertThrows(BadRequestException.class, () -> alterCharacterService.createCharacter(character));
    }

    @Test
    void testUpdateCharacter() {
        UUID id = UUID.randomUUID();
        Character requestCharacter = createHarryPotter();
        Character persistedCharacter = createHarryPotter();
        persistedCharacter.setRole(Role.TEACHER);

        when(repository.findById(any(UUID.class))).thenReturn(Optional.of(persistedCharacter));
        when(prepareService.prepareForUpdate(any(Character.class), any(Character.class))).thenReturn(requestCharacter);
        when(repository.save(any())).thenReturn(requestCharacter);

        assertDoesNotThrow(() -> alterCharacterService.updateCharacter(id, requestCharacter));
        verify(repository).save(characterCaptor.capture());
        Character value = characterCaptor.getValue();
        compareCharacterResults(requestCharacter, value);
    }

    @Test
    void testDeleteCharacterWithNotFoundException() {
        when(repository.findById(any(UUID.class))).thenReturn(Optional.empty());
        assertThrows(NotFoundException.class, () -> alterCharacterService.deleteCharacter(UUID.randomUUID()));
    }

    @Test
    void testDeleteCharacterWithBadRequestException() {
        Character character = createHarryPotter();
        when(repository.findById(any(UUID.class))).thenReturn(Optional.of(character));
        doThrow(new RuntimeException()).when(repository).delete(any(Character.class));
        assertThrows(BadRequestException.class, () -> alterCharacterService.deleteCharacter(UUID.randomUUID()));
    }

    @Test
    void testDeleteCharacter() {
        Character character = createHarryPotter();
        when(repository.findById(any(UUID.class))).thenReturn(Optional.of(character));
        assertDoesNotThrow(() -> alterCharacterService.deleteCharacter(UUID.randomUUID()));
        verify(repository, times(1)).delete(character);
    }
}