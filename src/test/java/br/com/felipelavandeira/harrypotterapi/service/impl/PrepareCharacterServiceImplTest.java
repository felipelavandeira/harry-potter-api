package br.com.felipelavandeira.harrypotterapi.service.impl;

import br.com.felipelavandeira.harrypotterapi.exceptionhandler.exception.NotFoundHouseException;
import br.com.felipelavandeira.harrypotterapi.model.entity.Character;
import br.com.felipelavandeira.harrypotterapi.model.repository.HouseRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static utils.CharacterComparator.compareCharacterResults;
import static utils.CharacterCreator.createHarryPotter;

@ExtendWith(MockitoExtension.class)
class PrepareCharacterServiceImplTest {

    @InjectMocks
    private PrepareCharacterServiceImpl service;

    @Mock
    private HouseRepository repository;

    @Test
    void testPrepareForUpdate() {
        Character harryPotter = new Character();
        Character changedHarryPotter = createHarryPotter();
        when(repository.findById(any(UUID.class))).thenReturn(Optional.of(changedHarryPotter.getHouse()));
        harryPotter = service.prepareForUpdate(changedHarryPotter, harryPotter);
        harryPotter.setId(changedHarryPotter.getId());
        compareCharacterResults(changedHarryPotter, harryPotter);

        Character allAttributesNullCharacter = new Character();
        harryPotter = service.prepareForUpdate(allAttributesNullCharacter, harryPotter);
        compareCharacterResults(changedHarryPotter, harryPotter);
    }

    @Test
    void testPrepareForCreate() {
        Character harryPotter = createHarryPotter();
        when(repository.findById(any(UUID.class))).thenReturn(Optional.of(harryPotter.getHouse()));
        Character result = assertDoesNotThrow(() -> service.prepareForCreate(harryPotter));
        compareCharacterResults(harryPotter, result);
    }

    @Test
    void testPrepareForUpdate_WithException() {
        Character harryPotter = createHarryPotter();
        when(repository.findById(any(UUID.class))).thenReturn(Optional.empty());
        assertThrows(NotFoundHouseException.class, () -> service.prepareForUpdate(harryPotter, harryPotter));
    }
}