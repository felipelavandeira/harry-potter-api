package br.com.felipelavandeira.harrypotterapi.service.impl;

import br.com.felipelavandeira.harrypotterapi.exceptionhandler.exception.NotFoundException;
import br.com.felipelavandeira.harrypotterapi.model.entity.Character;
import br.com.felipelavandeira.harrypotterapi.model.entity.House;
import br.com.felipelavandeira.harrypotterapi.model.repository.CharacterRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static utils.CharacterCreator.createHarryPotter;

@ExtendWith(MockitoExtension.class)
class RetrieveCharacterServiceImplTest {

    @InjectMocks
    private RetrieveCharacterServiceImpl service;

    @Mock
    private CharacterRepository repository;

    @Test
    void testRetrieveWithouFilter() {
        when(repository.findAll(any(Pageable.class))).thenReturn(Page.empty());
        assertDoesNotThrow(() -> service.retrieveList(PageRequest.of(1, 1), null));
        verify(repository).findAll(any(Pageable.class));
    }

    @Test
    void testRetrieveWithFilter() {
        when(repository.findByHouse(any(Pageable.class), any(House.class))).thenReturn(Page.empty());
        assertDoesNotThrow(() -> service.retrieveList(PageRequest.of(1, 1), UUID.randomUUID().toString()));
        verify(repository).findByHouse(any(Pageable.class), any(House.class));
    }

    @Test
    void testRetrieveOne_WithoutException() {
        Character harryPotter = createHarryPotter();
        when(repository.findById(any(UUID.class))).thenReturn(Optional.of(harryPotter));
        assertDoesNotThrow(() -> service.retrieveOne(UUID.randomUUID()));
    }

    @Test
    void testRetrieveOne_WithException() {
        when(repository.findById(any(UUID.class))).thenReturn(Optional.empty());
        assertThrows(NotFoundException.class, () -> service.retrieveOne(UUID.randomUUID()));
    }
}