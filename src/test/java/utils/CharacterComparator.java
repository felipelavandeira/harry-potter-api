package utils;

import br.com.felipelavandeira.harrypotterapi.model.entity.Character;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CharacterComparator {

    public static void compareCharacterResults(Character expected, Character result) {
        assertEquals(expected.getId(), result.getId());
        assertEquals(expected.getHouse(), result.getHouse());
        assertEquals(expected.getSchool(), result.getSchool());
        assertEquals(expected.getRole(), result.getRole());
        assertEquals(expected.getPatronus(), result.getPatronus());
        assertEquals(expected.getName(), result.getName());
    }

}
