package utils;

import br.com.felipelavandeira.harrypotterapi.model.entity.Character;
import br.com.felipelavandeira.harrypotterapi.model.entity.House;
import br.com.felipelavandeira.harrypotterapi.model.enumerated.Role;

import java.util.UUID;

public class CharacterCreator {

    public static Character createHarryPotter() {
        Character character = new Character();
        character.setId(UUID.randomUUID());
        House house = new House();
        house.setId(UUID.randomUUID());
        house.setName("Gryffindor");
        character.setHouse(house);
        character.setName("Harry Potter");
        character.setPatronus("expectro");
        character.setSchool("Hogwarts");
        character.setRole(Role.STUDENT);
        return character;
    }

}
